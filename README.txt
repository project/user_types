
1. What does it do ?

It gives you the power to make custom profiles for certain types of users. This module makes it possible to define custom form fields (profile module must be enabled). Then it let’s you determine for which profiles this field will be active. For example: If we want a field where you can enter who’s your favorite band, you create this field like you would normally do in admin/user/profile/add/textfield and at the bottom you can choose for which user types (which you defined earlier in admin/user/user_types)  this field will be active using the checkboxes. It’s that simple.

2.	How do you use it ?

a. Create, change and delete a user type

You start with creating user types in admin/user/user_types. In the list you’ll see which types you’ve created earlier. After clicking the second tab ‘add user type’ you can enter the name of the user type you want to create in the textfield. The third tab is used to name the ‘untyped‘ (i.e. users without a type) users. 
You can also change/delete the name of the type later, by clicking one of the types in the list. Clicking the edit tab will give the change/delete options. 

b. Create a custom form field for a type

You go to admin/user/profile and you chose what type of profile entry you want, choose a category to group the new one by (you‘ll have to create categories to group the fields together), a title, a form name, etc… At the bottom you will be able to choose for which types the field will be active. 
The last checkbox, ‘Show in quick view’ enabled will give you a view of the fields and their submitted data in quick view when viewing the list of all the users in a type (admin/user/user_types/type_name_example). When not enabled you will only see the field and its data when clicking detailed view (which you’ll see clicking the tab next to it). 

c. Add a user to a type

This can only be done by creating new users. Existing users cannot be given a type. Only new users can get a type. When creating an account, you choose which type you want to be and you fill in the custom fields for your type.
This causes a problem when you have created custom fields before you installed the user_type module and you want the earlier created fields to be part of one of the newly created types. This will cause existing users to loose the ability to update these fields. Since you can’t give a type to an existing user, and you’ve set the field to be associated with a user of the certain type, then that means that all submitted data won’t be accessible anymore by that user. The data is still accessible by the profile module and will be shown on the profile page. Reason: The user type module causes the user edit form to display nothing.

3. Credits

Originally developed by Neil Drumm for Drupal 5.
Now maintained by Kristof De Jaeger - http://drupal.org/user/107403

