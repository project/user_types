<?php

/**
 * @file
 * Different user types with customizable profile fields.
 *
 * @author Kristof De Jaeger - http://drupal.org/user/107403 - http://realize.be
 * Originally by Neil Drumm.
 * @version this is the drupal 5.x version
 */

/**
 * Implementation of hook_menu().
 */
function user_types_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/user/user_types',
      'title' => t('User types'),
      'description' => t('List, edit, and add user types.'),
      'access' => user_access('administer users'),
      'callback' => 'user_types_admin_page',
    );
    $items[] = array(
      'path' => 'admin/user/user_types/list',
      'title' => t('List'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/user/user_types/settings',
      'title' => t('Settings'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('user_types_settings'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 2,
    );
    $items[] = array(
      'path' => 'admin/user/user_types/add',
      'title' => t('Add user type'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('user_types_add_form'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 1,
    );

    foreach (user_types_list() as $user_type_id => $name) {
      $items[] = array(
        'path' => 'admin/user/user_types/'. check_plain($name),
        'title' => $name,
        'callback' => 'user_types_type_page',
        'callback arguments' => array($user_type_id),
      );
      $items[] = array(
        'path' => 'admin/user/user_types/'. check_plain($name) .'/list',
        'title' => t('List users'),
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );
      $items[] = array(
        'path' => 'admin/user/user_types/'. check_plain($name) .'/list/quick',
        'title' => t('Quickâ€view'),
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );
      $items[] = array(
        'path' => 'admin/user/user_types/'. check_plain($name) .'/list/detail',
        'title' => t('Detailâ€view'),
        'type' => MENU_LOCAL_TASK,
        'callback' => 'user_types_type_page',
        'callback arguments' => array($user_type_id, TRUE),
        'weight' => 1,
      );
      $items[] = array(
        'path' => 'admin/user/user_types/'. check_plain($name) .'/list/'. check_plain($name) .'.csv',
        'title' => t('CSV'),
        'type' => MENU_CALLBACK,
        'callback' => 'user_types_type_page',
        'callback arguments' => array($user_type_id, TRUE, TRUE),
      );
      $items[] = array(
        'path' => 'admin/user/user_types/'. check_plain($name) .'/edit',
        'title' => t('Edit'),
        'callback' => 'drupal_get_form',
        'callback arguments' => array('user_types_edit_form', $user_type_id),
        'type' => MENU_LOCAL_TASK,
        'weight' => 1,
      );
    }

    $items[] = array(
      'path' => 'admin/user/user_types/delete',
      'title' => t('Delete user type'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('user_types_delete_form'),
      'type' => MENU_CALLBACK,
    );
    foreach (array('user/register', 'admin/user/user/create') as $base_path) {
      $items[] = array(
        'path' => $base_path .'/untyped',
        'title' => variable_get('user_types_untyped_name', t('Normal user')),
        'type' => MENU_DEFAULT_LOCAL_TASK,
        'weight' => '-1',
      );
      foreach (user_types_list(TRUE) as $user_type_id => $name) {
        $items[] = array(
          'path' => $base_path .'/'. check_plain($name),
          'title' => $name,
          'type' => MENU_LOCAL_TASK,
        );
      }
    }
  }
  else {
    if (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == 'edit') {
      $items[] = array(
        'path' => 'user/'. arg(1) .'/edit/user_types',
        'title' => t('User type'),
        'callback' => 'drupal_get_form',
        'callback arguments' => array('user_types_user_edit', arg(1)),
        'access' => user_access('switch user types'),
        'type' => MENU_LOCAL_TASK,
        'weight' => 100,
      );
    }
  }

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function user_types_perm() {
  return array('switch user types');
}

/**
 * Implementation of hook_help().
 */
function user_types_help($section = NULL) {
  if (preg_match('!^user/[0-9]*/edit/user_types$!', $section)) {
    return '<p>'. t('Changing the user type will <strong>lose data</strong> in fields which are removed.') .'</p>';
  }
  if ($section == 'admin/help#user_types') {
    // we make it easy by reading in the README.txt
    $path = drupal_get_path('module', 'user_types');
    $readme = file_get_contents($path .'/README.txt');
    return '<p>'. nl2br($readme) .'</p>';
  }
}

function user_types_list($only_enabled = FALSE) {
  if ($only_enabled) {
    $result = db_query('SELECT user_type_id, name FROM {user_types_type} WHERE enable_registration ORDER BY name');
  }
  else {
    $result = db_query('SELECT user_type_id, name FROM {user_types_type} ORDER BY name');
  }
  $user_types = array();
  while ($row = db_fetch_object($result)) {
    $user_types[$row->user_type_id] = $row->name;
  }
  return $user_types;
}

/**
 * Get the current user type ID for create user pages.
 */
function user_types_add_page_id() {
  static $user_type_id;

  if (!isset($user_type_id)) {
    $menu = menu_get_menu();
    $user_type_id = db_result(db_query("SELECT user_type_id FROM {user_types_type} WHERE name = '%s'", $menu['items'][menu_get_active_item()]['title']));
    if (empty($user_type_id)) {
      $user_type_id = -1;
    }
  }

  return $user_type_id;
}

/**
 * Implementation of hook_form_alter().
 */
function user_types_form_alter($form_id, &$form) {
  switch ($form_id) {
    case 'profile_field_form':
      // Get list of user types
      $user_types = array(
      -1 => check_plain(variable_get('user_types_untyped_name', t('Normal user'))),
      );
      $user_types = $user_types + array_map('check_plain', user_types_list());

      // Get default value
      if (isset($form['fid'])) {
        $result = db_query('SELECT user_type_id, enabled FROM {user_types_profile_fields} WHERE fid = %d', $form['fid']['#value']);
        $default_value = array();
        while ($row = db_fetch_object($result)) {
          $default_value[$row->user_type_id] = $row->enabled * $row->user_type_id;
        }
      }
      else {
        $default_value = drupal_map_assoc(array(-1));
      }

      $form['fields']['user_types'] = array(
        '#type' => 'checkboxes',
        '#title' => t('User types'),
        '#options' => $user_types,
        '#default_value' => $default_value,
      );
      $form['fields']['user_types_quick_view'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show in quickâ€view'),
        '#default_value' => db_result(db_query('SELECT enabled FROM {user_types_quick_view} WHERE fid = %d', $form['fid']['#value'])),
      );

      $form['#submit']['user_types_profile_field_form_submit'] = array();
      break;

    case 'user_edit':
      $user_type_id = db_result(db_query('SELECT user_type_id FROM {user_types_user} WHERE uid = %d', $form['_account']['#value']->uid));
      if (empty($user_type_id)) {
        $user_type_id = -1;
      }
    case 'user_register':
      if (!isset($user_type_id)) {
        $user_type_id = user_types_add_page_id();
      }

      // Remove disabled fields for this type.
      $result = db_query('SELECT pf.name, pf.category, utpf.enabled FROM {user_types_profile_fields} utpf INNER JOIN {profile_fields} pf ON pf.fid = utpf.fid WHERE utpf.user_type_id = %d', $user_type_id);
      $categories = array();
      while ($row = db_fetch_object($result)) {
        if (!$row->enabled) {
          unset($form[$row->category][$row->name]);
          $categories[] = $row->category;
        }
      }

      // Remove empty fieldsets.
      foreach (array_unique($categories) as $category) {
        if (count(element_children($form[$category])) == 0) {
          if ($form_id == 'user_edit' && isset($form[$category])) {
            // We can not remove a category added by profile module, so the
            // best we can do is confirm the page is supposed to be empty.
            drupal_set_message(t('There are no fields in this category.'));
          }
          unset($form[$category]);
        }
      }
      break;
  }
}

/**
 * Implementation of hook_user().
 */
function user_types_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'insert':
      db_query('INSERT INTO {user_types_user} (uid, user_type_id) VALUES (%d, %d)', $account->uid, user_types_add_page_id());
      break;
  }
}

function user_types_profile_field_form_submit($form_id, $form_values) {
  if (isset($form_values['fid'])) {
    $fid = $form_values['fid'];
  }
  else {
    $fid = db_result(db_query("SELECT fid FROM {profile_fields} WHERE name = '%s'", $form_values['name']));
  }

  db_query('DELETE FROM {user_types_profile_fields} WHERE fid = %d', $fid);
  foreach ($form_values['user_types'] as $user_type_id => $enabled) {
    db_query('INSERT INTO {user_types_profile_fields} (user_type_id, fid, enabled) VALUES (%d, %d, %d)', $user_type_id, $fid, (bool) $enabled);
  }

  db_query('DELETE FROM {user_types_quick_view} WHERE fid = %d', $fid);
  db_query('INSERT INTO {user_types_quick_view} (fid, enabled) VALUES (%d, %d)', $fid, $form_values['user_types_quick_view']);
}

function user_types_admin_page() {
  $output = '';

  $user_types = user_types_list();
  if (count($user_types) > 0) {
    $output .= '<ul>';
    foreach ($user_types as $user_type_id => $name) {
      $output .= '<li>'. l($name, 'admin/user/user_types/'. check_plain($name)) .'</li>';
    }
    $output .= '</ul>';
  }
  else {
    $output .= '<p><em>'. t('There are no user types. You can <a href="!url">add a user type</a>.', array('!url' => url('admin/user/user_types/add'))) .'</em></p>';
  }

  return $output;
}

function user_types_settings() {
  $form = array();

  $form['user_types_untyped_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name for untyped users'),
    '#default_value' => variable_get('user_types_untyped_name', t('Normal user')),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  return $form;
}

function user_types_settings_submit($form_id, $form_values) {
  variable_set('user_types_untyped_name', $form_values['user_types_untyped_name']);
  cache_clear_all('*', 'cache_menu', TRUE);
  drupal_set_message('Saved user types settings.');
  return 'admin/user/user_types';
}

function user_types_add_form() {
  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  );
  $form['enable_registration'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable registration'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add user type'),
  );

  return $form;
}

function user_types_add_form_submit($form_id, $form_values) {
  db_query("INSERT INTO {user_types_type} (user_type_id, name, enable_registration) VALUES (%d, '%s', %d)", db_next_id('{user_types_type}_user_type_id'), $form_values['name'], $form_values['enable_registration']);
  cache_clear_all('*', 'cache_menu', TRUE);
  drupal_set_message(t('Added %name user type.', array('%name' => $form_values['name'])));
  return 'admin/user/user_types/'. check_plain($form_values['name']);
}

function user_types_edit_form($user_type_id) {
  $user_type = db_fetch_object(db_query('SELECT user_type_id, name, enable_registration FROM {user_types_type} WHERE user_type_id = %d', $user_type_id));

  $form = array();

  $form['user_type_id'] = array(
    '#type' => 'value',
    '#value' => $user_type->user_type_id,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $user_type->name,
    '#required' => TRUE,
  );
  $form['enable_registration'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable registration'),
    '#default_value' => $user_type->enable_registration,
  );
  $form[] = array('#value' => '<div class="container-inline">');
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save user type'),
  );
  $form['delete'] = array(
    '#value' => l(t('Delete'), 'admin/user/user_types/delete/'. $user_type->user_type_id),
  );
  $form[] = array('#value' => '</div>');

  return $form;
}

function user_types_edit_form_submit($form_id, $form_values) {
  db_query("UPDATE {user_types_type} SET name = '%s', enable_registration = %d WHERE user_type_id = %d", $form_values['name'], $form_values['enable_registration'], $form_values['user_type_id']);
  cache_clear_all('*', 'cache_menu', TRUE);
  drupal_set_message(t('Saved %name user type.', array('%name' => $form_values['name'])));
  return 'admin/user/user_types/'. check_plain($form_values['name']);
}

function user_types_delete_form($user_type_id) {
  $user_type = db_fetch_object(db_query('SELECT user_type_id, name FROM {user_types_type} WHERE user_type_id = %d', $user_type_id));
  if (empty($user_type)) {
    drupal_set_message(t('Invalid user type.'), 'error');
    return array();
  }

  $form = array();

  $form['user_type_id'] = array(
    '#type' => 'value',
    '#value' => $user_type->user_type_id,
  );
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $user_type->name,
  );

  return confirm_form($form, t('Are you sure you want to delete the %name user type?', array('%name' => $user_type->name)), 'admin/user/user_types/edit/'. $user_type->user_type_id);
}

function user_types_delete_form_submit($form_id, $form_values) {
  db_query("DELETE FROM {user_types_type} WHERE user_type_id = %d", $form_values['user_type_id']);
  cache_clear_all('*', 'cache_menu', TRUE);
  drupal_set_message(t('Deleted %name user type.', array('%name' => $form_values['name'])));
  return 'admin/user/user_types';
}

function user_types_type_page($user_type_id, $detail_view = FALSE, $csv = FALSE) {
  $output = '';

  $joins = array();
  if (!$detail_view) {
    $joins[] = 'INNER JOIN {user_types_quick_view} utqv ON utpf.fid = utqv.fid AND utqv.enabled';
  }
  $orders = array(
    'pf.category',
    'pf.weight',
    'pf.fid',
  );
  foreach (module_implements('user_types_table_alter') as $module) {
    // Use pass by reference in implementations. Useful for reordering columns.
    $function = $module .'_user_types_table_alter';
    $function($joins, $orders);
  }
  $result = db_query('SELECT pf.name, pf.title, pf.fid, pf.category FROM {user_types_profile_fields} utpf INNER JOIN {profile_fields} pf ON pf.fid = utpf.fid '. implode(' ', $joins) .' WHERE utpf.enabled AND utpf.user_type_id = %d ORDER BY '. implode(', ', $orders), $user_type_id);
  $header = array(
  array(
      'data' => t('User'),
      'field' => 'u.name',
      'sort' => 'asc',
  ),
  );
  $fields = array();
  while ($row = db_fetch_object($result)) {
    $header[] = array(
      'data' => $row->title,
      'field' => 'pv'. $row->fid .'.value',
      'category' => $row->category,
    );
    $fields[] = $row->name;
  }

  $tablesort_order = tablesort_get_order($header);
  $join = '';
  if (preg_match('/^pv([0-9]*)/', $tablesort_order['sql'], $matches)) {
    $join = 'INNER JOIN {profile_values} pv'. $matches[1] .' ON pv'. $matches[1] .'.uid = utu.uid AND pv'. $matches[1] .'.fid = '. $matches[1];
  }
  if ($csv) {
    $result = db_query('SELECT utu.uid FROM {user_types_user} utu INNER JOIN {users} u ON u.uid = utu.uid '. $join .' WHERE utu.user_type_id = %d'. tablesort_sql($header), $user_type_id);
  }
  else {
    $result = pager_query('SELECT utu.uid FROM {user_types_user} utu INNER JOIN {users} u ON u.uid = utu.uid '. $join .' WHERE utu.user_type_id = %d'. tablesort_sql($header), 20, 0, NULL, $user_type_id);
  }

  $rows = array();
  while ($row = db_fetch_object($result)) {
    $account = user_load(array('uid' => $row->uid));
    $r = array(
    theme('username', $account),
    );
    foreach ($fields as $field) {
      $r[] = $account->$field;
    }
    $rows[] = $r;
  }

  if ($csv) {
    drupal_set_header('Content-type: text/csv');

    // Category header
    $categories = array_map(create_function('$item', 'return $item[\'category\'];'), $header);
    $categories[0] = t('Category');
    $output .= user_types_to_csv($categories);

    // Field header
    $output .= user_types_to_csv(array_map(create_function('$item', 'return $item[\'data\'];'), $header));

    // Row data
    foreach ($rows as $row) {
      $output .= user_types_to_csv($row);
    }
    print $output;
  }
  else {
    if (count($rows) > 0) {
      $output .= theme('table', $header, $rows);
    }
    else {
      $output .= '<p><em>'. t('There are no users of this type. You can <a href="!url">add a user</a>.', array('!url' => url('admin/user/user/create/'. drupal_get_title()))) .'</em></p>';
    }
    $output .= theme('pager', 20, 0);
    $output .= l(t('Download CSV'), 'admin/user/user_types/'. arg(3) .'/list/'. arg(3) .'.csv');

    return $output;
  }
}

function user_types_to_csv($array) {
  $output = '"'. user_types_quote_csv(array_shift($array)) .'"';
  foreach ($array as $item) {
    $output .= ',"'. user_types_quote_csv($item) .'"';
  }
  $output .= "\n";

  return $output;
}

function user_types_quote_csv($string) {
  $string = str_replace('"', '""', strip_tags($string));
  $string = str_replace("\r\n", "\n", $string);
  $string = str_replace("\r", "\n", $string);

  return $string;
}

function user_types_user_edit($uid) {
  $account = user_load(array('uid' => $uid));
  drupal_set_title($account->name);
  $user_type_id = db_result(db_query('SELECT user_type_id FROM {user_types_user} WHERE uid = %d', $uid));

  $user_types = array(
  -1 => check_plain(variable_get('user_types_untyped_name', t('Normal user'))),
  );
  $user_types = $user_types + array_map('check_plain', user_types_list());

  $form = array();

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $uid,
  );
  $form['user_type'] = array(
    '#type' => 'select',
    '#title' => t('User type'),
    '#options' => $user_types,
    '#default_value' => $user_type_id,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Change user type'),
  );

  return $form;
}

function user_types_user_edit_submit($form_id, $form_values) {
  $user_types = array(
  -1 => check_plain(variable_get('user_types_untyped_name', t('Normal user'))),
  );
  $user_types = $user_types + array_map('check_plain', user_types_list());

  db_query('UPDATE {user_types_user} SET user_type_id = %d WHERE uid = %d', $form_values['user_type'], $form_values['uid']);
  drupal_set_message(t('Changed user type to %type.', array('%type' => $user_types[$form_values['user_type']])));
  return 'user/'. $form_values['uid'];
}
